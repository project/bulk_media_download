<?php
namespace Drupal\bulk_media_download\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure example settings for this site.
 */
class BulkDownloadSettingsForm extends ConfigFormBase {

  /** 
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'bulkDownloadForm.settings';

  /** 
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bulk_media_download_settings';
  }

  /** 
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /** 
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['content_types'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Media Types'),
      '#options' => [
        'image' => "IMAGE",
        'audio' => "AUDIO",
        'video' => "VIDEO"

      ],
      '#default_value' => $config->get('content_types'),
    ];  

    // $form['media_types'] = [
    //   '#type' => 'textfield',
    //   '#title' => $this->t('Other things'),
    //   '#default_value' => $config->get('other_things'),
    // ];  

    return parent::buildForm($form, $form_state);
  }

  /** 
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Retrieve the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      // Set the submitted configuration setting.
      ->set('content_types', $form_state->getValue('content_types'))
      // You can set multiple configurations at once by making
      // multiple calls to set().
      // ->set('other_things', $form_state->getValue('other_things'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}