# Bulk Media Download

This module will provide the functionality to download all of
the media as per desired node type.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/bulk_media_download).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/bulk_media_download).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

The module currently provides no configuration options.


## Maintainers

- Sushant Dhungel - [Sushant-D](https://www.drupal.org/u/sushant-d)